package com.covidTest.request;

import com.covidTest.response.StatusResponse;

public class TestResult extends StatusResponse {
	private String sample;
	private int personId;
	
	
	

	public int getPersonId() {
		return personId;
	}

	public void setPersonId(int personId) {
		this.personId = personId;
	}

	public String getSample() {
		return sample;
	}

	public void setSample(String sample) {
		this.sample = sample;
	}
	
	

}
