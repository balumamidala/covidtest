package com.covidTest.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.Calendar;

import com.covidTest.request.PersonDetails;
import com.covidTest.request.TestResult;

public class PersonJdbc {
	public int insertPersonDetails(PersonDetails personDetails) {

		int personId = 0;

		try {
			DbConnection dbConnection = new DbConnection();
			Connection connection = dbConnection.getConnection();
			Timestamp timestamp = getTime();

			String query = "insert into person_details (person_id,first_name,last_name,phone_number,adhar_number,mandal,date_time,status) values(?,?,?,?,?,?,?,?)";

			PreparedStatement statement = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
			statement.setInt(1, personDetails.getPersonId());
			statement.setString(2, personDetails.getFirstName());
			statement.setString(3, personDetails.getLastName());
			statement.setString(4, personDetails.getPhoneNumber());
			statement.setString(5, personDetails.getAdharNumber());
			statement.setString(6, personDetails.getMandal());
			statement.setTimestamp(7, timestamp);
			statement.setString(8, "not activated");

			int count = statement.executeUpdate();
			if (count == 1) {
				ResultSet rs = statement.getGeneratedKeys();
				if (rs.next()) {
					personId = rs.getInt(1);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return personId;

	}

	private Timestamp getTime() {
		Calendar calendar = Calendar.getInstance();
		Timestamp timeStamp = new Timestamp(calendar.getTimeInMillis());
		return timeStamp;
	}

	public boolean setStatus(TestResult testResult) {
		boolean recordInserted = false;
		try {

			DbConnection dbConnection = new DbConnection();
			Connection connection = dbConnection.getConnection();
			String query = "update  person_details  set status =? where person_id=" + testResult.getPersonId();

			PreparedStatement statement = connection.prepareStatement(query);
			statement.setString(1, "activated");
			int count = statement.executeUpdate();
			if (count == 1) {

				String query1 = "insert into test_center(person_id,sample)values(?,?)";
				PreparedStatement statement1 = connection.prepareStatement(query1);
				statement1.setInt(1, testResult.getPersonId());
				statement1.setString(2, testResult.getSample());

				int count1 = statement1.executeUpdate();
				if (count1 == 1) {
					recordInserted = true;
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return recordInserted;
	}

	public TestResult getResult(PersonDetails personDetails, TestResult testResult) {

		
		try {

			DbConnection dbConnection = new DbConnection();
			Connection connection = dbConnection.getConnection();

			String query = "select * from person_details p Join test_center t on p.person_id = t.person_id where adhar_number="+"'"+personDetails.getAdharNumber()+"'";

			PreparedStatement statement = connection.prepareStatement(query);
			
			
			ResultSet rs = statement.executeQuery();
			while (rs.next()) {
				
					int personId = rs.getInt("person_id");
					String sample = rs.getString("sample");

					testResult.setPersonId(personId);
					testResult.setSample(sample);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return testResult;
	}
}
