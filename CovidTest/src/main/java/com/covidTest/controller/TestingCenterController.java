package com.covidTest.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.covidTest.jdbc.PersonJdbc;
import com.covidTest.request.TestResult;
import com.covidTest.response.StatusResponse;
@CrossOrigin
@RestController
public class TestingCenterController {
	@PostMapping("/person-status")
	public StatusResponse updatePersonStatus(@RequestBody TestResult testResult){
		PersonJdbc personJdbc = new PersonJdbc();
		StatusResponse statusResponse = new StatusResponse();
		boolean statusOutput = personJdbc.setStatus( testResult);
		if (statusOutput == true) {
			statusResponse.setStatus("success");
			statusResponse.setStatusText("updated successfully");

		} else {
			statusResponse.setStatus("failed");
			statusResponse.setStatusText("failed to update");
		}
		return statusResponse;
	}

}
