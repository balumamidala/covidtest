package com.covidTest.controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.covidTest.jdbc.PersonJdbc;
import com.covidTest.request.PersonDetails;
import com.covidTest.request.TestResult;
import com.covidTest.response.PersonResponse;
import com.covidTest.response.StatusResponse;

@RestController
@CrossOrigin
public class CovidPersonController {
	@PostMapping("/person")
	public StatusResponse insertPersonDetails(@RequestBody PersonDetails personDetails) {
		PersonJdbc personJdbc = new PersonJdbc();
		//StatusResponse statusResponse = new StatusResponse();
		PersonResponse personResponse = new PersonResponse();
		int personId = personJdbc.insertPersonDetails(personDetails);
		if(personId!=0) {
			personResponse.setStatus("success");
			personResponse.setStatusText("records inserted successfully");
			personResponse.setPersonId(personId);
		}else {
			personResponse.setStatus("failed");
			personResponse.setStatusText("failed to insert");
		}
		return personResponse;
		
	}
	
	@PostMapping("/result")
	public TestResult getResult(@RequestBody PersonDetails personDetails, TestResult testResult) {
		PersonJdbc personJdbc = new PersonJdbc();
		TestResult personTestResult = personJdbc.getResult(personDetails, testResult);
		
		if(personTestResult != null && personTestResult.getSample()!=null) {
			testResult.setStatus("success");
			testResult.setStatusText("records found");
			testResult.setPersonId(personTestResult.getPersonId());
			testResult.setSample(personTestResult.getSample());
			
			
		}else {
			testResult.setStatus("failed");
			testResult.setStatusText("records not found, result not updated");
		}
		return testResult;
		
		
		
	}

}
