package com.covidTest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CovidTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(CovidTestApplication.class, args);
	}

}
